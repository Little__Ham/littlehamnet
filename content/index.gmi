```
                                        ,,,,,,,,
              ,,,amwmmmmww,,,,        ]KKKKKKKKKNWw
          ,,mK                `Km,    ]KKKKKKKKKKKKKM
       ,mK                        `W, KKKKKKKKKKKKK"
     ,#                              KKKKKKKKKKKKK
    ;                           jKKKNp "KKKKKKKKM
   ,                              "9KKKw, ``"""M
  (                                   `"MKK#mmKBKKN,
  (                                          `""MKM*`
  (                                              K
   V                                             |
  ,#          ,,wwpppw,            ,,, ,      ,,'
dH$|Ppw,wnpbh@hhmhhhh@hhb            `"`    ,@|||
h@hhhhhhhh@h@PPPPPhhPP@hPK                 /|||H|K,
0h@@hPPPPPPPPhPhPPPPhPPhhPb,,w,          ,0||||||||Kw,
 "K@@$@PPPPP@@PhhhPPPPPPhPKbhbhbK`""""*MM"""KK|||||||||N
   `*K@hP$PPPhP$0hPPhPPhK'KM`KM`M              ``""*MM""
       `""MKKKKhPPBKM*^
```

# Little Ham's den
Just a small piece of ham doing their thing around the internet, trying to switch to FOSS and the small internet where possible.
I barely publish anything I do, but my head keeps flying around new tech-related projects every once in a while.

## What I'm up to
=> https://git.littleham.net Git repositories
=> /shared.gmi Shared files

## Where to find me
### Links
=> gemini://littleham.net Gemini capsule
=> gopher://littleham.net/ Gopher hole
=> https://littleham.net Website

### Contact
=> /pgp.gmi Mail/PGP
=> gemini://station.martinrue.com/little_ham Station
=> https://fosstodon.org/@Little__Ham Mastodon

## Explore
=> gemini://gemini.circumlunar.space Project Gemini
=> gemini://geminispace.info Geminispace
=> gemini://medusae.space Medusae
=> gemini://astrobotany.mozz.us Astrobotany
=> gemini://iich.space IIch
=> https://en.wikipedia.org/wiki/Gopher_(protocol) Gopher Protocol
=> gopher://gopher.floodgap.com/v2 Veronica-2
=> gopher://bitreich.org/ Bitreich
