# littleham.net
* [Capsule](gemini://littleham.net)
* [Website](https://littleham.net)
* [Hole](gopher://littleham.net)

This repository houses the original gmi files, server and home-made conversion scripts to gph and html formats.
