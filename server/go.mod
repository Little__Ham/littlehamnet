module littleham.net

go 1.17

require github.com/pitr/gig v0.9.8

require (
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.1.0 // indirect
)
