package main

import "github.com/pitr/gig"

const wdir="/home/ham/gig/littleham.net/"
const cdir="/home/ham/gemini/"

func main() {
	/// Capsules
	domains := map[string]*gig.Gig{}

	/// littleham.net
	a := gig.Default()
	domains["littleham.net"] = a
	a.Static("/", cdir+"littleham.net")
	
	// Gone
	a.Handle("/gemlog.gmi", func(c gig.Context) error {
		return gig.ErrGone
	})
	a.Handle("/entries/21-10-13.gmi", func(c gig.Context) error {
		return gig.ErrGone
	})

	/// git.littleham.net
	b := gig.Default()
	domains["git.littleham.net"] = b
	b.Static("/", cdir+"git.littleham.net")


	/// Server
	g := gig.New()
	g.Handle("/*", func(c gig.Context) error {
		domain := domains[c.URL().Host]

		if domain == nil {
			return gig.ErrNotFound
		}

		domain.ServeGemini(c)
		return nil
	})

	g.Run(wdir+"littleham.crt", wdir+"littleham.key")

}
